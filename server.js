const bodyParser = require("body-parser")
const express = require("express")
const cors = require("cors")
const { createCanvas } = require('canvas')
var barcode = require('barcode');
const JsBarcode = require('jsbarcode')
const port = 6700
const app = express()
app.use(
  cors({
    origin: true,
  })
)

app.use(express.static(__dirname))
app.use(bodyParser.json({ limit: "50mb" }))
app.use(
  bodyParser.urlencoded({
    limit: "50mb",
    extended: true,
    parameterLimit: 50000,
  })
)
app.route('/').get((req,res)=> res.send('hello'))
app.route("/generate/barcode/:textId/:ref1/:ref2/:amount").get((req, res) => {
  try {
    const { textId, ref1, ref2, amount } = req.params
    let barcodeData = "|"
    barcodeData += textId + "\r"
    barcodeData += ref1 + "\r"
    barcodeData += ref2 + "\r"
    barcodeData += amount + "\r"
   
    
    console.log({ barcodeData })
    const canvas = new createCanvas()
    JsBarcode(canvas, barcodeData, {
        text: ' '
    })
    res.contentType("image/jpeg")
    res.end(canvas.toBuffer())
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
  }
})
app.listen(port, () => {
  console.log(`Server running port ${port} ......`)
})
