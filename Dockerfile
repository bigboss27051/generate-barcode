# Dockerfile
# 1st Stage
FROM node:10.16.0-alpine

RUN apk add tzdata     
RUN npm install knex -g
RUN npm install pm2 -g

RUN mkdir -p C://Users/bigbo/work/test:/usr/src/app
WORKDIR C://Users/bigbo/work/test:/usr/src/app

COPY package.json /

#  Install dependencies
RUN apk add --update \
&& apk add --no-cache ffmpeg opus pixman cairo pango giflib ca-certificates \
&& apk add --no-cache --virtual .build-deps git curl build-base jpeg-dev pixman-dev \
cairo-dev pango-dev pangomm-dev libjpeg-turbo-dev giflib-dev freetype-dev python g++ make \
\
# Install node.js dependencies
&& yarn install --build-from-source \
\
# Clean up build dependencies
&& apk del .build-deps

# Add project source
COPY . .
RUN npm install
# 2nd Stage
CMD ["pm2-runtime", "--json", "server.js"]
